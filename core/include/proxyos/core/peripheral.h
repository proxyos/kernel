#pragma once

#include <stdint.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

struct peripheral_t;

typedef void (*peripheral_hal_declaration_t)(void const* peripheral, char* it, char** next);
typedef void (*peripheral_hal_callback_t)(void* peripheral, char const* type, char const* it);
typedef void (*peripheral_hal_start_t)(void* peripheral);
typedef void (*peripheral_hal_stop_t)(void* peripheral);

typedef struct peripheral_t {
  int32_t id;
  char const* type;
  char const* name;
  void* hal;
  peripheral_hal_declaration_t declaration;
  peripheral_hal_callback_t callback;
  peripheral_hal_start_t start;
  peripheral_hal_stop_t stop;
  TaskHandle_t task;
} peripheral_t;

void
peripheral_declare(peripheral_t const* peripheral, char* it, char** next);
