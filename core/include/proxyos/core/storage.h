#pragma once

#include <stddef.h>

int
storage_set(char const* name, char const* data, size_t len);

int
storage_get_size(char const* name, size_t* len);

int
storage_get(char const* name, char* output, size_t len);
