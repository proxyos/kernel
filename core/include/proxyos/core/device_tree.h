#pragma once

#include "./peripheral.h"

peripheral_t* const*
device_tree_load(char const* dt);

void
device_tree_status(void);