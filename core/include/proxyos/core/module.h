#pragma once

#include "./peripheral.h"

#define MODULE_DECLARE(_type, _constructor, _destructor) \
  {                                                      \
    static module_t __module = {                         \
        .type        = #_type,                           \
        .constructor = _constructor,                     \
        .destructor  = _destructor,                      \
    };                                                   \
                                                         \
    __module_declare(&__module);                         \
  }

typedef int (*module_contructor_t)(peripheral_t* peripheral, char const* it);
typedef int (*module_destructor_t)(peripheral_t* peripheral);

typedef struct {
  char const* type;
  module_contructor_t constructor;
  module_destructor_t destructor;
} module_t;

int
__module_declare(module_t* module);

void
module_status(void);

module_t*
module_find_by_name(char const* name);
