#pragma once

#include <stddef.h>

typedef void (*message_send_callback_t)(char const* buffer, size_t len);

void
message_set_send_callback(message_send_callback_t cb);

void
message_send(char const* buffer, size_t len);

void
message_write_header(char* it, char const* type, char** next);

struct peripheral_t;

void
message_write_peripheral_header(
    char* it,
    char const* type,
    struct peripheral_t const* peripheral,
    char** next);
