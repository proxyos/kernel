#include "proxyos/core/logger.h"

#include <sys/time.h>

#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include <bson.h>

static char const* level_str[] = {"err", "wng", "inf", "dbg"};

static inline int
print_header(unsigned level, char const* tag, char const* func) {
  struct timespec tms = {0};
  clock_gettime(CLOCK_REALTIME, &tms);

  long unsigned millis = tms.tv_sec * 1000 + tms.tv_nsec / 1000000;
  if (tms.tv_nsec % 1000000 >= 500000) ++millis;
  millis %= 1000000000;

  ssize_t total = 0;

  {
    ssize_t written = printf(
        "%010lu:%s(%s)",
        millis,
        tag,
        level < (sizeof(level_str) / sizeof(*level_str)) ? level_str[level] : "---");
    if (written < 0) return written;
    total += written;
  }

  // Function name if any
  {
    int written = func ? printf("@%s: ", func) : printf(": ");
    if (written < 0) return written;
    total += written;
  }

  return total;
}

void
__logger_va(unsigned level, char const* tag, char const* func, char const* fmt, va_list args) {
  if (print_header(level, tag, func) < 0) return;
  if (vprintf(fmt, args) < 0) return;
  printf("\n");
  fsync(STDOUT_FILENO);
}

void
__logger(unsigned level, char const* tag, char const* func, char const* fmt, ...) {
  va_list argptr;
  va_start(argptr, fmt);
  __logger_va(level, tag, func, fmt, argptr);
  va_end(argptr);
}

void
__logger_bson(unsigned level, char const* tag, char const* func, char const* bson) {
  if (print_header(level, tag, func) < 0) return;
  printf("\n");
  bson_print(bson, 13, 2);
  printf("\n");
  fsync(STDOUT_FILENO);
}
