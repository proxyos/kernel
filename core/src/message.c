#include "proxyos/core/message.h"

#include <string.h>

#include <bson.h>

#include "proxyos/core/peripheral.h"

static message_send_callback_t send = NULL;

void
message_set_send_callback(message_send_callback_t cb) {
  send = cb;
}

void
message_send(char const* buffer, size_t len) {
#if 0
  bson_print(buffer, 0, 2);
  printf("\n");

  for (int i = 0; i < len;) {
    for (int j = 0; j < 10 && i < len; ++i, ++j) { printf("%02hhx ", buffer[i]); }
    printf("\n");
  }
#endif

  if (send) send(buffer, len);
}

void
message_write_header(char* it, char const* type, char** next) {
  // type
  {
    bson_set_element_type(it, BSON_STRING, &it);
    bson_set_element_name(it, "tp", strlen("tp"), &it);
    bson_set_element_value_string(it, type, strlen(type), &it);
  }

  if (next) *next = it;
}

void
message_write_peripheral_header(
    char* it,
    char const* type,
    peripheral_t const* peripheral,
    char** next) {
  message_write_header(it, type, &it);

  // source
  {
    bson_set_element_type(it, BSON_INT32, &it);
    bson_set_element_name(it, "pr", strlen("pr"), &it);
    bson_set_element_value_int32(it, peripheral->id, &it);
  }

  if (next) *next = it;
}
