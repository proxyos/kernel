#include "proxyos/core/peripheral.h"

#include <string.h>

#include <bson.h>

void
peripheral_declare(peripheral_t const* peripheral, char* it, char** next) {
  // type
  {
    bson_set_element_type(it, BSON_STRING, &it);
    bson_set_element_name(it, "tp", strlen("tp"), &it);
    bson_set_element_value_string(it, peripheral->type, strlen(peripheral->type), &it);
  }

  // name
  {
    bson_set_element_type(it, BSON_STRING, &it);
    bson_set_element_name(it, "nm", strlen("nm"), &it);
    bson_set_element_value_string(it, peripheral->name, strlen(peripheral->name), &it);
  }

  // id
  {
    bson_set_element_type(it, BSON_INT32, &it);
    bson_set_element_name(it, "id", strlen("id"), &it);
    bson_set_element_value_int32(it, peripheral->id, &it);
  }

  if (next) *next = it;
}