#include "proxyos/core/device_tree.h"

#include <string.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#include "proxyos/core/module.h"

static char const* const TAG = "device_tree";

#define PERIPHERAL_MAX_COUNT 64

static peripheral_t* peripherals[PERIPHERAL_MAX_COUNT] = {0};
static int peripheral_last                             = 0;

static void
peripheral_free_all(void) {
  if (peripheral_last > 0) {
    for (int i = 0; i < peripheral_last; ++i) {
      if (peripherals[i]->task) {
        LOGD(TAG, "Deleting task for '%s'", peripherals[i]->name);
        vTaskDelete(peripherals[i]->task);
      }

      module_t* module = module_find_by_name(peripherals[i]->type);
      if (!module) {
        LOGE(TAG, "Unable to find module of type '%s'", peripherals[i]->type);
        continue;
      }

      if (module->destructor) {
        LOGD(TAG, "Destroying '%s'", peripherals[i]->name);
        module->destructor(peripherals[i]);
      } else {
        LOGW(TAG, "No destructor set for '%s'", peripherals[i]->name);
      }

      free((void*) peripherals[i]->name);
      free(peripherals[i]);
      peripherals[i] = NULL;
    }

    peripheral_last = 0;
  }
}

static int
peripheral_load(char const* it, char const* name) {
  LOGI(TAG, "Loading peripheral '%s'", name);

  // tp
  char const* tp = NULL;
  {
    bson_element_t type = bson_get_element_type(it, &it);
    uint32_t name_size;
    char const* name = bson_get_element_name(it, &name_size, &it);

    if (type != BSON_STRING) {
      LOGE(TAG, "First field should be of type BSON_STRING but got %d", (int) type);
      return 1;
    }

    if (strcmp(name, "tp") != 0) {
      LOGE(TAG, "First field should be a string named 'tp' but got '%s'", name);
      return 2;
    }

    tp = bson_get_element_value_string(it, NULL, &it);
  }

  module_t* module = module_find_by_name(tp);
  if (!module) {
    LOGE(TAG, "No module of type '%s'", tp);
    return 3;
  }

  if (peripheral_last >= PERIPHERAL_MAX_COUNT) {
    LOGE(TAG, "Unable to instantiate peripheral '%s', please increase PERIPHERAL_MAX_COUNT", name);
    return 4;
  }

  peripheral_t* peripheral = malloc(sizeof(peripheral_t));
  memset(peripheral, 0, sizeof(*peripheral));

  size_t name_len  = strlen(name);
  peripheral->name = malloc(name_len + 1);
  memcpy((void*) peripheral->name, name, name_len + 1);

  peripheral->type = module->type;
  peripheral->id   = peripheral_last;

  int ret = module->constructor(peripheral, it);
  if (ret != 0) {
    LOGE(TAG, "Failed to contruct peripheral '%s'", name);
    free((void*) peripheral->name);
    free(peripheral);
    return 5;
  }

  peripherals[peripheral_last] = peripheral;
  ++peripheral_last;

  return 0;
}

peripheral_t* const*
device_tree_load(char const* dt) {
  // Ensure starting from scratch
  peripheral_free_all();

  LOGBSOND(TAG, dt);

  // Loading device tree
  {
    char const* it = dt + sizeof(uint32_t);

    bson_element_t type = bson_get_element_type(it, &it);
    while (type != BSON_END) {
      if (type != BSON_OBJECT) {
        LOGE(TAG, "Device tree element should be BSON_OBJECT but got %d", (int) type);
        return NULL;
      }

      uint32_t name_size = 0;
      char const* name   = bson_get_element_name(it, &name_size, &it);
      {
        char const* obj;
        uint32_t size = bson_get_size(it, &obj);
        int ret       = peripheral_load(obj, name);
        if (ret != 0) LOGE(TAG, "Failed to load peripheral '%s' (ret=%d)", name, ret);

        it += size;
      }

      type = bson_get_element_type(it, &it);
    }
  }

  return peripherals;
}

void
device_tree_status(void) {
  LOGI(TAG, "Peripherals begin");
  for (int i = 0; i < peripheral_last; ++i) {
    LOGI(TAG, "  -> Peripheral '%s' of type '%s'", peripherals[i]->name, peripherals[i]->type);
  }
  LOGI(TAG, "Peripherals end");
}
