#include "proxyos/core/system.h"

#include <esp_system.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

static char const* const TAG = "system";

void
system_restart(void) {
  LOGI(TAG, "Restarting");
  esp_restart();
}