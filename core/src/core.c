#include "proxyos/core/core.h"

#include <nvs.h>
#include <nvs_flash.h>

#include <driver/gpio.h>

static void
initialize_nvs(void) {
  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    err = nvs_flash_init();
  }
  ESP_ERROR_CHECK(err);
}

void
proxyos_core_init(void) {
  initialize_nvs();
  gpio_install_isr_service(0);
}