#include "proxyos/core/storage.h"

#include <esp_err.h>

#include <nvs.h>
#include <nvs_flash.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

static char const* const TAG = "storage";
static nvs_handle_t storage_handle;

static int
storage_open(void) {
  esp_err_t err = nvs_open(TAG, NVS_READWRITE, &storage_handle);
  if (err != ESP_OK) { LOGW(TAG, "Unable to open NVS: %s", esp_err_to_name(err)); }
  return err;
}

static void
storage_close(void) {
  nvs_close(storage_handle);
}

int
storage_set(char const* name, char const* data, size_t len) {
  if (storage_open() != 0) return 2;

  int ret = 0;
  if (nvs_set_blob(storage_handle, name, data, len) != ESP_OK) {
    LOGW(TAG, "Unable to set '%s' into storage", name);
    ret = 1;
  }

  storage_close();

  return ret;
}

int
storage_get_size(char const* name, size_t* len) {
  if (storage_open() != 0) return 2;

  int ret = 0;
  if (nvs_get_blob(storage_handle, name, NULL, len) != ESP_OK) {
    LOGW(TAG, "Unable to get size of '%s'", name);
    ret = 1;
  }

  storage_close();

  return ret;
}

int
storage_get(char const* name, char* output, size_t len) {
  if (storage_open() != 0) return 2;

  int ret = 0;
  if (nvs_get_blob(storage_handle, name, output, &len) != ESP_OK) {
    LOGW(TAG, "Unable to get '%s' value", name);
    ret = 1;
  }

  storage_close();

  return ret;
}
