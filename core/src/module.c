#include "proxyos/core/module.h"

#include <string.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

static char const* const TAG = "module";

#define MODULE_MAX_COUNT 64

/////////////
// MODULES //
/////////////

static module_t* modules[MODULE_MAX_COUNT] = {0};
static int module_last                     = 0;

int
__module_declare(module_t* module) {
  if (module_last >= MODULE_MAX_COUNT) {
    LOGE(TAG, "Unable to declare module '%s', please increase MODULE_MAX_COUNT", module->type);
    return -1;
  }

  modules[module_last] = module;
  ++module_last;

  return 0;
}

void
module_status(void) {
  LOGI(TAG, "Modules begin");
  for (int i = 0; i < module_last; ++i) { LOGI(TAG, "  -> Module '%s'", modules[i]->type); }
  LOGI(TAG, "Modules end");
}

module_t*
module_find_by_name(char const* name) {
  for (int j = 0; j < module_last; ++j) {
    if (strcmp(name, modules[j]->type) == 0) { return modules[j]; }
  }

  return NULL;
}
