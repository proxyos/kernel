#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct servo_t {
  int32_t gpio;
  int32_t frequency;

  int32_t min_duty; // in us
  int32_t max_duty; // in us
  int32_t max_degree;

  void* private;
} servo_t;

int
servo_init(
    servo_t* this,
    int32_t gpio,
    int32_t frequency,
    int32_t min_duty,
    int32_t max_duty,
    double max_degree);

void
servo_deinit(servo_t* this);

void
servo_set_degree(servo_t const* this, double degree_of_rotation);
