#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
  int32_t gpio;
  char const* unit;

  bool enabled;

  void* private;
} ntc_t;

int
ntc_init(ntc_t* this, int32_t gpio, char const* unit);

void
ntc_deinit(ntc_t* this);

int32_t
ntc_read(ntc_t const* this);
