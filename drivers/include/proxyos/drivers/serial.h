#pragma once

#include <sys/types.h>

#include <stddef.h>
#include <stdint.h>

int
serial_init(void);

void
serial_purge(void);

void
serial_write_bytes(char const* data, size_t len);

ssize_t
serial_read_bytes(char* data, size_t len, size_t time_ms);

void
serial_set_speed(int32_t speed);

int32_t
serial_get_speed(void);

////////////////
// Unsafe API //
////////////////

// The following function are not thread safe, only use it if not possible otherwise.

void
serial_write_bytes_unsafe(char const* data, size_t len);

void
serial_write_mutex_take(void);

void
serial_write_mutex_give(void);

ssize_t
serial_read_bytes_unsafe(char* data, size_t len, size_t time_ms);

void
serial_read_mutex_take(void);

void
serial_read_mutex_give(void);
