#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct {
  int32_t gpio;
  char const* color;

  int32_t max_pwm;

  void* private;
} led_t;

int
led_init(led_t* this, int32_t gpio, char const* color);

void
led_deinit(led_t* this);

void
led_set_duty(led_t* this, uint32_t target_duty);

void
led_set_duty_with_fade(led_t* this, uint32_t target_duty, uint32_t time_ms);