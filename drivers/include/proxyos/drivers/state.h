#pragma once

typedef enum {
  STATE_OFF = 0,
  STATE_ON  = 1,
} state_t;
