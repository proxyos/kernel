#include "proxyos/core/module.h"
#include "proxyos/core/peripheral.h"

#include "./led/peripheral.h"
#include "./ntc/peripheral.h"
#include "./servo/peripheral.h"

void
proxyos_drivers_init(void) {
  led_module_declare();
  ntc_module_declare();
  servo_module_declare();
}
