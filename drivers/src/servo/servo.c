#include "proxyos/drivers/servo.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <esp_attr.h>

#include <driver/mcpwm.h>
#include <soc/mcpwm_periph.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#define THIS ((servo_esp32_t*) this->private)

static char const* const TAG = "servo";

typedef struct {
  mcpwm_unit_t mcpwm_num;
  mcpwm_io_signals_t io_signal;
  mcpwm_config_t pwm_config;
} servo_esp32_t;

int
servo_init(
    servo_t* this,
    int32_t gpio,
    int32_t frequency,
    int32_t min_duty,
    int32_t max_duty,
    double max_degree) {
  LOGD(
      TAG,
      "servo_init gpio=%d frequency=%d min_duty=%d max_duty=%d max_degree=%f",
      (int) gpio,
      (int) frequency,
      (int) min_duty,
      (int) max_duty,
      (float) max_degree);

  memset(this, 0, sizeof(*this));
  this->gpio       = gpio;
  this->frequency  = frequency;
  this->min_duty   = min_duty;
  this->max_duty   = max_duty;
  this->max_degree = max_degree;

  this->private = malloc(sizeof(*THIS));
  memset(this->private, 0, sizeof(*THIS));

  static mcpwm_unit_t mcpwm_num       = 0;
  static mcpwm_io_signals_t io_signal = 0;

  while (true) {
    if (mcpwm_num < MCPWM_UNIT_MAX) {
      THIS->mcpwm_num = mcpwm_num;

      if (io_signal <= MCPWM2B) {
        THIS->io_signal = io_signal;
        ++io_signal;
        break;
      } else {
        LOGW(TAG, "No more MCPWM signals available on unit %d", mcpwm_num);
        ++mcpwm_num;
        io_signal = 0;
      }
    } else {
      LOGE(TAG, "No more MCPWM units available");
      goto fail_free_private;
    }
  }

  mcpwm_gpio_init(THIS->mcpwm_num, THIS->io_signal, gpio);

  THIS->pwm_config.frequency    = frequency;
  THIS->pwm_config.cmpr_a       = 0;
  THIS->pwm_config.cmpr_b       = 0;
  THIS->pwm_config.counter_mode = MCPWM_UP_COUNTER;
  THIS->pwm_config.duty_mode    = MCPWM_DUTY_MODE_0;
  mcpwm_init(THIS->mcpwm_num, MCPWM_TIMER_0, &THIS->pwm_config);

  return 0;

fail_free_private:
  servo_deinit(this);
  return 1;
}

void
servo_deinit(servo_t* this) {
  if (this) {
    if (THIS) free(THIS);
    memset(this, 0, sizeof(*this));
  }
}

void
servo_set_degree(servo_t const* this, double degree_of_rotation) {
  uint32_t pulsewidth_us = 0;
  pulsewidth_us =
      (this->min_duty +
       (((this->max_duty - this->min_duty) * (degree_of_rotation)) / (this->max_degree)));

  mcpwm_set_duty_in_us(THIS->mcpwm_num, MCPWM_TIMER_0, MCPWM_OPR_A, pulsewidth_us);
  vTaskDelay(10);
}
