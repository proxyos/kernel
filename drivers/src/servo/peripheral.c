#include "./peripheral.h"

#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#include "proxyos/core/message.h"
#include "proxyos/core/module.h"
#include "proxyos/core/peripheral.h"

#include "proxyos/drivers/servo.h"

static char const* const TAG = "servo.peripheral";

void
servo_declare(void const* peripheral, char* it, char** next) {
  servo_t const* hal = (servo_t const*) ((peripheral_t const*) peripheral)->hal;

  // max_degree
  {
    bson_set_element_type(it, BSON_DOUBLE, &it);
    bson_set_element_name(it, "max_degree", strlen("max_degree"), &it);
    bson_set_element_value_double(it, hal->max_degree, &it);
  }

  if (next) *next = it;
}

static void
servo_callback(void* peripheral, char const* type, char const* it) {
  servo_t* hal = (servo_t*) ((peripheral_t*) peripheral)->hal;

  LOGD(TAG, "Received request of type '%s'", type);

  if (strcmp(type, "set_degree") == 0) {
    int32_t value;
    {
      bson_element_t tp = bson_get_element_type(it, &it);
      if (tp != BSON_DOUBLE) {
        LOGE(TAG, "Invalid element type: %d. Should be: %d", (int) tp, (int) BSON_DOUBLE);
        return;
      }

      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      if (strcmp(name, "value") != 0) {
        LOGE(TAG, "Invalid element name: '%s'. Should be 'value'", name);
        return;
      }

      value = bson_get_element_value_double(it, &it);
    }

    servo_set_degree(hal, value);
  }
}

static void
servo_start(void* peripheral) {
  LOGD(TAG, "Starting servo");
  servo_t* hal = (servo_t*) ((peripheral_t*) peripheral)->hal;
  servo_set_degree(hal, 0);
}

static void
servo_stop(void* peripheral) {}

static int
servo_contructor(peripheral_t* peripheral, char const* it) {
  servo_t* hal = (servo_t*) malloc(sizeof(servo_t));
  memset(hal, 0, sizeof(servo_t));
  peripheral->hal = hal;

  peripheral->declaration = servo_declare;
  peripheral->start       = servo_start;
  peripheral->stop        = servo_stop;
  peripheral->callback    = servo_callback;

  int32_t gpio      = -1;
  int32_t frequency = -1;
  int32_t min_duty  = -1;
  int32_t max_duty  = -1;
  double max_degree = -1;
  {
    bson_element_t type = bson_get_element_type(it, &it);
    while (type != BSON_END) {
      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      /****/ if (strcmp(name, "gpio") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "servo.gpio should be a BSON_INT32");
          goto fail_free_hal;
        }

        gpio = bson_get_element_value_int32(it, &it);

        LOGD(TAG, "Setting servo '%s' gpio to %d", peripheral->name, gpio);
      } else if (strcmp(name, "frequency") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "servo.frequency should be a BSON_INT32");
          goto fail_free_hal;
        }

        frequency = bson_get_element_value_int32(it, &it);

        LOGD(TAG, "Setting servo '%s' frequency to %d", peripheral->name, frequency);
      } else if (strcmp(name, "min_duty") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "servo.min_duty should be a BSON_INT32");
          goto fail_free_hal;
        }

        min_duty = bson_get_element_value_int32(it, &it);

        LOGD(TAG, "Setting servo '%s' min_duty to %d", peripheral->name, min_duty);
      } else if (strcmp(name, "max_duty") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "servo.max_duty should be a BSON_INT32");
          goto fail_free_hal;
        }

        max_duty = bson_get_element_value_int32(it, &it);

        LOGD(TAG, "Setting servo '%s' max_duty to %d", peripheral->name, max_duty);
      } else if (strcmp(name, "max_degree") == 0) {
        if (type != BSON_DOUBLE) {
          LOGE(TAG, "servo.max_degree should be a BSON_DOUBLE");
          goto fail_free_hal;
        }

        max_degree = bson_get_element_value_double(it, &it);

        LOGD(TAG, "Setting servo '%s' max_degree to %d", peripheral->name, max_degree);
      }

      type = bson_get_element_type(it, &it);
    }
  }

  if (gpio < 0 || frequency < 0 || min_duty >= max_duty || max_degree < 0) {
    LOGE(
        TAG,
        "Servo should have valid "
        "'gpio', 'frequency', 'min_duty', 'max_duty' and 'max_degree' "
        "fields");
    goto fail_free_hal;
  }

  servo_init(hal, gpio, frequency, min_duty, max_duty, max_degree);
  return 0;

fail_free_hal:
  servo_deinit(hal);
  free(hal);

  memset(peripheral, 0, sizeof(*peripheral));

  return 1;
}

static int
servo_destructor(peripheral_t* peripheral) {
  servo_t* hal = (servo_t*) peripheral->hal;

  free(hal);
  peripheral->hal = NULL;

  return 0;
}

void
servo_module_declare(void) {
  MODULE_DECLARE(servo, servo_contructor, servo_destructor);
}
