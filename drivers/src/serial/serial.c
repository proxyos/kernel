#include "proxyos/drivers/serial.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <esp_err.h>

#include <driver/gpio.h>
#include <driver/uart.h>

#include <nvs_flash.h>

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

static char const* TAG = "serial";
#define STORAGE_NAMESPACE "serial"

//#define DEBUG_MUTEX_READ
//#define DEBUG_MUTEX_WRITE

#define SERIAL_TXD (GPIO_NUM_1)
#define SERIAL_RXD (GPIO_NUM_3)
#define SERIAL_RTS (UART_PIN_NO_CHANGE)
#define SERIAL_CTS (UART_PIN_NO_CHANGE)

void
serial_set_speed(int32_t speed) {
  nvs_handle_t nvs_handle;
  {
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
      LOGE(TAG, "Unable to open storage %s", STORAGE_NAMESPACE);
      return;
    }
  }

  esp_err_t err = nvs_set_i32(nvs_handle, "speed", speed);
  if (err != ESP_OK) { LOGE(TAG, "Failed to save speed"); }

  LOGI(TAG, "Set serial speed to %d", (int) speed);

  nvs_close(nvs_handle);
}

static const int32_t default_speed = 115200;

int32_t
serial_get_speed(void) {
  nvs_handle_t nvs_handle;
  {
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
      LOGE(TAG, "Unable to open storage %s", STORAGE_NAMESPACE);
      goto default_speed;
    }
  }

  int32_t speed = 0;
  {
    esp_err_t err = nvs_get_i32(nvs_handle, "speed", &speed);
    if (err != ESP_OK) {
      LOGI(TAG, "No speed found in NVS");
      goto register_default_speed;
    }
  }

  nvs_close(nvs_handle);
  return speed;

register_default_speed:
  nvs_close(nvs_handle);
  LOGI(TAG, "Setting speed to default value: %d", default_speed);
  serial_set_speed(default_speed);

default_speed:
  return default_speed;
}

static SemaphoreHandle_t mutex_write = NULL;
static SemaphoreHandle_t mutex_read  = NULL;

static int
serial_read_and_print(void) {
  serial_read_mutex_take();
  char c;
  int ret = serial_read_bytes_unsafe(&c, sizeof(c), portMAX_DELAY);
  serial_read_mutex_give();

  if (ret < 0) return ret;
  LOGI(TAG, "0x%02x ", c);
  return 0;
}

static void
read_until_no_more(void) {
  serial_read_mutex_take();
  while (true) {
    char c;
    int ret = serial_read_bytes_unsafe(&c, sizeof(c), 0);
    if (ret == 0) break;
    if (ret < 0) { LOGE(TAG, "Failed to purge serial queue"); }
  }
  serial_read_mutex_give();
}

void
serial_purge(void) {
  read_until_no_more();
}

esp_err_t
serial_init(void) {
  uart_config_t serial_config = {
      .baud_rate = serial_get_speed(),
      .data_bits = UART_DATA_8_BITS,
      .parity    = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  esp_err_t err = uart_param_config(UART_NUM_0, &serial_config);
  if (err != ESP_OK) {
    LOGE(TAG, "Failed to set config: %s", esp_err_to_name(err));
    return err;
  }

  err = uart_set_pin(UART_NUM_0, SERIAL_TXD, SERIAL_RXD, SERIAL_RTS, SERIAL_CTS);
  if (err != ESP_OK) {
    LOGE(TAG, "Failed to set pin: %s", esp_err_to_name(err));
    return err;
  }

  static size_t const stack_size = 4096;

  err = uart_driver_install(UART_NUM_0, stack_size, 0, 0, NULL, 0);
  if (err != ESP_OK) {
    LOGE(TAG, "Failed to install driver: %s", esp_err_to_name(err));
    return err;
  }

  mutex_write = xSemaphoreCreateMutex();
  if (!mutex_write) { LOGE(TAG, "Unable to init write mutex"); }

  mutex_read = xSemaphoreCreateMutex();
  if (!mutex_read) { LOGE(TAG, "Unable to init read mutex"); }

  return ESP_OK;
}

void
serial_write_bytes_unsafe(char const* data, size_t len) {
  uart_write_bytes(UART_NUM_0, data, len);
}

void
serial_write_mutex_take(void) {
#ifdef DEBUG_MUTEX_WRITE
  LOGW(TAG, "Try to take write mutex");
#endif

  if (mutex_write) xSemaphoreTake(mutex_write, portMAX_DELAY);

#ifdef DEBUG_MUTEX_WRITE
  LOGW(TAG, "Write mutex taken");
#endif
}

void
serial_write_mutex_give(void) {
  if (mutex_write) xSemaphoreGive(mutex_write);

#ifdef DEBUG_MUTEX_WRITE
  LOGW(TAG, "Write mutex given");
#endif
}

ssize_t
serial_read_bytes_unsafe(char* data, size_t len, size_t time_ms) {
  return uart_read_bytes(
      UART_NUM_0,
      (uint8_t*) data,
      len,
      time_ms == portMAX_DELAY ? portMAX_DELAY : time_ms / portTICK_RATE_MS);
}

void
serial_read_mutex_take(void) {
#ifdef DEBUG_MUTEX_READ
  LOGW(TAG, "Try to take read mutex");
#endif

  if (mutex_read) xSemaphoreTake(mutex_read, portMAX_DELAY);

#ifdef DEBUG_MUTEX_READ
  LOGW(TAG, "Read mutex taken");
#endif
}

void
serial_read_mutex_give(void) {
  if (mutex_read) xSemaphoreGive(mutex_read);

#ifdef DEBUG_MUTEX_READ
  LOGW(TAG, "Read mutex given");
#endif
}

void
serial_write_bytes(char const* data, size_t len) {
  serial_write_mutex_take();
  LOGD(TAG, "0x%02x 0x%02x 0x%02x 0x%02x", data[0], data[1], data[2], data[3]);
  serial_write_bytes_unsafe(data, len);
  serial_write_mutex_give();
}

ssize_t
serial_read_bytes(char* data, size_t len, size_t time_ms) {
  serial_read_mutex_take();
  ssize_t ret = serial_read_bytes_unsafe(data, len, time_ms);
  serial_read_mutex_give();
  return ret;
}
