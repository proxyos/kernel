#include "proxyos/drivers/ntc.h"

#include <limits.h>
#include <math.h>
#include <string.h>

#include <driver/adc.h>

#include <esp_adc_cal.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

static char const* const TAG = "ntc";

//#define USE_CALIBRATION

#define NB_MEASUREMENTS 10
#define ADC_MAX_VAL ((1 << 12) - 1)
#define RESISTOR 10000
#define V_IN (3.3f)
#define DEFAULT_VREF 1100

#define NB_ADC_UNITS 2

#define THIS ((ntc_esp32_t*) this->private)

typedef struct {
  adc_channel_t channel;
  int port;
} ntc_esp32_t;

#ifdef USE_CALIBRATION

static esp_adc_cal_value_t calibration_mode[NB_ADC_UNITS];
static esp_adc_cal_characteristics_t calibration_characteristics[NB_ADC_UNITS];

static void
ntc_calibration_init(void) {
  for (int adc_port = 0; adc_port < NB_ADC_UNITS; adc_port++) {
    calibration_mode[adc_port] = esp_adc_cal_characterize(
        ADC_UNIT_1 + adc_port,
        ADC_ATTEN_DB_11,
        ADC_WIDTH_BIT_12,
        DEFAULT_VREF,
        &calibration_characteristics[adc_port]);

    switch (calibration_mode[adc_port]) {
      case ESP_ADC_CAL_VAL_EFUSE_VREF: {
        LOGD(TAG, "Using EFUSE VREF vfor ADC port %d.", adc_port + 1);
        break;
      }

      case ESP_ADC_CAL_VAL_EFUSE_TP: {
        LOGD(TAG, "Using EFUSE Two-points value for calibration on ADC port %d.", adc_port + 1);
        break;
      }

      default: {
        LOGD(TAG, "Using default Vref value for calibration on ADC port %d.", adc_port + 1);
        break;
      }
    }
  }
}

#endif // USE_CALIBRATION

int
ntc_init(ntc_t* this, int32_t gpio, char const* unit) {
#ifdef USE_CALIBRATION
  static bool init_calib = false;
  if (!init_calib) {
    ntc_calibration_init();
    init_calib = true;
  }
#endif // USE_CALIBRATION

  memset(this, 0, sizeof(*this));

  this->private = malloc(sizeof(*THIS));
  memset(this->private, 0, sizeof(*THIS));

  THIS->channel = -1;
  THIS->port    = -1;
  {
    for (adc_channel_t channel = 0; channel < ADC_CHANNEL_MAX; ++channel) {
      gpio_num_t io = -1;

      if ((int) channel < ADC1_CHANNEL_MAX && ESP_OK == adc1_pad_get_io_num(channel, &io)) {
        if (gpio == io) {
          THIS->channel = channel;
          THIS->port    = 1;
          break;
        }
      }
      LOGD(TAG, "ADC1(%d) = %d", (int) channel, (int) io);

      if ((int) channel < ADC2_CHANNEL_MAX && ESP_OK == adc2_pad_get_io_num(channel, &io)) {
        if (gpio == io) {
          THIS->channel = channel;
          THIS->port    = 2;
          break;
        }
      }
      LOGD(TAG, "ADC2(%d) = %d", (int) channel, (int) io);
    }
  }

  if (THIS->channel == -1 || THIS->port == -1) {
    LOGE(TAG, "Unable to find ADC on GPIO%d", gpio);
    goto fail_free_hal;
  }

  if (unit) {
    size_t unit_len = strlen(unit);
    this->unit      = (char*) malloc(unit_len + 1);
    memcpy((void*) this->unit, unit, unit_len + 1);
  }

  switch (THIS->port) {
    case 1: {
      adc1_config_width(ADC_WIDTH_BIT_12);
      adc1_config_channel_atten(THIS->channel, ADC_ATTEN_DB_11);
      break;
    }

    case 2: {
      adc2_config_channel_atten(THIS->channel, ADC_ATTEN_DB_11);
      break;
    }

    default: {
      LOGE(TAG, "Unknown ADC port: %d", THIS->port);
      break;
    }
  }

  return 0;

fail_free_hal:
  ntc_deinit(this);
  return 1;
}

void
ntc_deinit(ntc_t* this) {
  if (this) {
    if (THIS) free(THIS);
    if (this->unit) free((void*) this->unit);
    memset(this, 0, sizeof(*this));
  }
}

static int32_t
read_celsius(ntc_t const* this) {
  float sum_temp_deg      = 0.f;
  float sum_vout_calib    = 0.f;
  float sum_vout_no_calib = 0.f;

#ifdef USE_CALIBRATION
  bool use_calibration =
      (ESP_ADC_CAL_VAL_EFUSE_TP == calibration_mode[this->port - 1] ||
       ESP_ADC_CAL_VAL_EFUSE_VREF == calibration_mode[this->port - 1]);
#endif

  for (int i = 0; i < NB_MEASUREMENTS; ++i) {
    int adc_value = 0;

    switch (THIS->port) {
      case 1: {
        adc_value = adc1_get_raw(THIS->channel);
        break;
      }

      case 2: {
        esp_err_t err = adc2_get_raw(THIS->channel, ADC_WIDTH_BIT_12, &adc_value);
        if (err != ESP_OK) return CHAR_MIN;
        break;
      }

      default: {
        LOGE(TAG, "Unknown ADC port: %d", THIS->port);
        return CHAR_MIN;
      }
    }

    if (adc_value == 0) return CHAR_MIN;

    LOGD(TAG, "ADC Value : %d", adc_value);

    float v_out_no_calib = ((float) adc_value * 3.3f / 4096.f);
    LOGD(TAG, "Uncalibrated voltage : %.3fv", v_out_no_calib);

    float v_out = v_out_no_calib;
    sum_vout_no_calib += v_out_no_calib;

#ifdef USE_CALIBRATION
    if (use_calibration) {
      float v_out_calib =
          esp_adc_cal_raw_to_voltage(adc_value, &calibration_characteristics[this->port - 1]) /
          1000.f;
      LOGD(TAG, "v_out Cal  : %.3fv", v_out_calib);

      sum_vout_calib += v_out_calib;

      // When available, use calibrated value instead of uncalibrated one.
      v_out = v_out_calib;
    }
#endif // USE_CALIBRATION

    float voltage_ratio = (float) v_out / (float) V_IN;
    float resistor_coef = (float) 1 / ((1.f / voltage_ratio) - 1.f);
    LOGD(TAG, "resistor_coeff : %d", (uint32_t) resistor_coef);

    /* The polynomial trendline is a 4th order. */
    /* 65,5 + -55,6x + 18,8x^2 + -2,96x^3 + 0,167x^4 */
    /* It provides temperature in °C by measuring the ratio of NTC resistance and 10k resistance.*/
    /* The NTC resistance NTC is obtained by making a voltage divider bridge. */
    /* Depending on the ratio obtained, we determine the temperature from the polynomial trend curve. */
    /* The coefficients are obtained so as to have an accurate measurement between -15 and 50 degrees. */
    /* The determination of the coefficients is found in the excel file firmware/study/ntc_sensor_study.xslsx */
    sum_temp_deg += 65.5f - 55.6f * resistor_coef + 18.8f * pow(resistor_coef, 2) -
                    2.96f * pow(resistor_coef, 3) + 0.167f * pow(resistor_coef, 4);
  }
  LOGD(TAG, "temperature_average : %d", (int32_t)(sum_temp_deg / (float) NB_MEASUREMENTS));

  // For debug purpose only
  LOGD(
      TAG,
      "voltage_average (Calibrated)  : %.2f",
      (float) (sum_vout_calib / (float) NB_MEASUREMENTS));
  LOGD(
      TAG,
      "voltage_average (Uncalibrated): %.2f",
      (float) (sum_vout_no_calib / (float) NB_MEASUREMENTS));

  return sum_temp_deg / (float) NB_MEASUREMENTS;
}

int32_t
ntc_read(ntc_t const* this) {
  if (strcmp(this->unit, "C") == 0) return read_celsius(this);

  LOGE(TAG, "Unhandled unit '%s'", this->unit);
  return 0;
}
