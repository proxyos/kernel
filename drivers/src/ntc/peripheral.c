#include "./peripheral.h"

#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#include "proxyos/core/message.h"
#include "proxyos/core/module.h"
#include "proxyos/core/peripheral.h"

#include "proxyos/drivers/ntc.h"

static char const* const TAG = "ntc.peripheral";

void
ntc_task(void* peripheral) {
  peripheral_t* ntc_peripheral = (peripheral_t*) peripheral;
  ntc_t* ntc_sensor            = (ntc_t*) ntc_peripheral->hal;

  int8_t temperature = 0;

  char* message = (char*) malloc(256);
  char* it      = message + sizeof(uint32_t);
  message_write_peripheral_header(it, "temperature", ntc_peripheral, &it);

  char* value = NULL;
  {
    bson_set_element_type(it, BSON_INT32, &it);
    bson_set_element_name(it, "value", strlen("value"), &it);
    value = it;
    bson_set_element_value_int32(it, temperature, &it);
  }

  bson_set_element_type(it, BSON_END, &it);
  uint32_t message_size = it - message;
  bson_set_size(message, message_size, NULL);

  while (true) {
    if (ntc_sensor->enabled) {
      int32_t new_val = ntc_read(ntc_sensor);
      if (temperature != new_val) {
        temperature = new_val;

        bson_set_element_value_int32(value, temperature, NULL);
        message_send(message, message_size);
      }
    }

    vTaskDelay(1000 / portTICK_RATE_MS);
  }

  free(message);
  vTaskDelete(NULL);
}

void
ntc_declare(void const* peripheral, char* it, char** next) {
  ntc_t const* hal = (ntc_t const*) ((peripheral_t const*) peripheral)->hal;

  // unit
  if (hal->unit) {
    bson_set_element_type(it, BSON_STRING, &it);
    bson_set_element_name(it, "unit", strlen("unit"), &it);
    bson_set_element_value_string(it, hal->unit, strlen(hal->unit), &it);
  }

  if (next) *next = it;
}

void
ntc_start(void* peripheral) {
  LOGD(TAG, "Starting ntc");
  ntc_t* hal   = (ntc_t*) ((peripheral_t*) peripheral)->hal;
  hal->enabled = true;
}

void
ntc_stop(void* peripheral) {
  LOGD(TAG, "Stoping ntc");
  ntc_t* hal   = (ntc_t*) ((peripheral_t*) peripheral)->hal;
  hal->enabled = false;
}

static int
ntc_contructor(peripheral_t* peripheral, char const* it) {
  ntc_t* hal = (ntc_t*) malloc(sizeof(ntc_t));
  memset(hal, 0, sizeof(ntc_t));
  peripheral->hal = hal;

  peripheral->declaration = ntc_declare;
  peripheral->start       = ntc_start;
  peripheral->stop        = ntc_stop;

  char const* unit = NULL;
  int32_t gpio     = -1;
  {
    bson_element_t type = bson_get_element_type(it, &it);
    while (type != BSON_END) {
      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      /****/ if (strcmp(name, "unit") == 0) {
        if (type != BSON_STRING) {
          LOGE(TAG, "ntc.unit should by a BSON_STRING");
          goto fail_free_hal;
        }

        uint32_t unit_len = 0;
        unit              = bson_get_element_value_string(it, &unit_len, &it);
        LOGD(TAG, "Setting ntc '%s' unit to '%s'", peripheral->name, unit);
      } else if (strcmp(name, "gpio") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "ntc.unit should by a BSON_INT32");
          goto fail_free_hal;
        }

        gpio = bson_get_element_value_int32(it, &it);

        LOGD(TAG, "Setting ntc '%s' gpio to %d", peripheral->name, gpio);
      }

      type = bson_get_element_type(it, &it);
    }
  }

  ntc_init(hal, gpio, unit);

  char buffer[configMAX_TASK_NAME_LEN] = {0};
  snprintf(buffer, sizeof(buffer), "%s:%s", peripheral->type, peripheral->name);
  xTaskCreate(ntc_task, peripheral->name, 4096, peripheral, 8, &peripheral->task);
  return 0;

fail_free_hal:
  free(hal);
  if (hal->unit) free((void*) hal->unit);
  memset(peripheral, 0, sizeof(*peripheral));

  return 1;
}

static int
ntc_destructor(peripheral_t* peripheral) {
  ntc_t* hal = (ntc_t*) peripheral->hal;

  if (hal->unit) free((void*) hal->unit);

  free(hal);
  peripheral->hal = NULL;

  return 0;
}

void
ntc_module_declare(void) {
  MODULE_DECLARE(ntc, ntc_contructor, ntc_destructor);
}
