#include "proxyos/drivers/led.h"

#include <stdio.h>
#include <string.h>

#include <driver/ledc.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#define THIS ((led_esp32_t*) this->private)

static const char* TAG = "led";

typedef struct {
  ledc_timer_config_t ledc_timer;
  ledc_channel_config_t ledc_channel;
} led_esp32_t;

int
led_init(led_t* this, int32_t gpio, char const* color) {
  LOGD(TAG, "led_init");
  memset(this, 0, sizeof(*this));

  static bool ok = false;
  if (!ok) {
    ledc_fade_func_install(0);
    ok = true;
  }

  this->private = malloc(sizeof(*THIS));
  memset(this->private, 0, sizeof(*THIS));

  THIS->ledc_timer.duty_resolution = LEDC_TIMER_13_BIT;
  THIS->ledc_timer.freq_hz         = 5000;
  THIS->ledc_timer.speed_mode      = LEDC_HIGH_SPEED_MODE;

  static ledc_timer_t timer = 0;
  if (timer < LEDC_TIMER_MAX) {
    THIS->ledc_timer.timer_num = timer;
    ++timer;
  } else {
    LOGE(TAG, "No more timer available");
    goto fail_free_private;
  }

  THIS->ledc_timer.clk_cfg = LEDC_AUTO_CLK;

  static ledc_channel_t channel = 0;
  if (channel < LEDC_CHANNEL_MAX) {
    THIS->ledc_channel.channel = channel;
    ++channel;
  } else {
    LOGE(TAG, "No more channel available");
    goto fail_free_private;
  }

  THIS->ledc_channel.duty       = 0;
  THIS->ledc_channel.speed_mode = THIS->ledc_timer.speed_mode;
  THIS->ledc_channel.hpoint     = 0;
  THIS->ledc_channel.timer_sel  = THIS->ledc_timer.timer_num;
  THIS->ledc_channel.gpio_num   = gpio;

  this->max_pwm =
      THIS->ledc_timer.duty_resolution > 0 ? ((1 << THIS->ledc_timer.duty_resolution) - 1) : 0;

  // Apply the LEDC PWM timer configuration
  ledc_timer_config(&THIS->ledc_timer);
  // Set LED Controller with prepared configuration
  ledc_channel_config(&THIS->ledc_channel);

  this->gpio = gpio;

  if (color) {
    size_t color_len = strlen(color);
    this->color      = (char const*) malloc(color_len + 1);
    memcpy((void*) this->color, color, color_len + 1);
  }

  return 0;

fail_free_private:
  led_deinit(this);
  return 1;
}

void
led_deinit(led_t* this) {
  if (this) {
    if (THIS) free(THIS);
    if (this->color) free((void*) this->color);
    memset(this, 0, sizeof(*this));
  }
}

void
led_set_duty_with_fade(led_t* this, uint32_t target_duty, uint32_t time_ms) {
  LOGD(
      TAG,
      "led_set_duty_with_fade target_duty=%lu time_ms=%lu",
      (long unsigned) target_duty,
      (long unsigned) time_ms);

  ledc_set_fade_with_time(
      THIS->ledc_channel.speed_mode, THIS->ledc_channel.channel, target_duty, time_ms);
  ledc_fade_start(THIS->ledc_channel.speed_mode, THIS->ledc_channel.channel, LEDC_FADE_NO_WAIT);
}

void
led_set_duty(led_t* this, uint32_t target_duty) {
  LOGD(TAG, "led_set_duty target_duty=%lu", (long unsigned) target_duty);

  ledc_set_duty(THIS->ledc_channel.speed_mode, THIS->ledc_channel.channel, target_duty);
  ledc_update_duty(THIS->ledc_channel.speed_mode, THIS->ledc_channel.channel);
}
