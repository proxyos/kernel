#include "./peripheral.h"

#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bson.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include "proxyos/core/logger.h"

#include "proxyos/core/module.h"
#include "proxyos/core/peripheral.h"

#include "proxyos/drivers/led.h"

static const char* TAG = "led.peripheral";

static void
led_declare(void const* peripheral, char* it, char** next) {
  led_t const* hal = (led_t const*) ((peripheral_t const*) peripheral)->hal;

  // color
  if (hal->color) {
    bson_set_element_type(it, BSON_STRING, &it);
    bson_set_element_name(it, "color", strlen("color"), &it);
    bson_set_element_value_string(it, hal->color, strlen(hal->color), &it);
  }

  // max_pwm
  {
    bson_set_element_type(it, BSON_INT32, &it);
    bson_set_element_name(it, "max_pwm", strlen("max_pwm"), &it);
    int32_t max_pwm = hal->max_pwm;
    bson_set_element_value_int32(it, max_pwm, &it);
  }

  if (next) *next = it;
}

static void
led_callback(void* peripheral, char const* type, char const* it) {
  led_t* hal = (led_t*) ((peripheral_t*) peripheral)->hal;

  LOGD(TAG, "Received request of type '%s'", type);

  if (strcmp(type, "set_pwm") == 0) {
    int32_t value;
    {
      bson_element_t tp = bson_get_element_type(it, &it);
      if (tp != BSON_INT32) {
        LOGE(TAG, "Invalid element type: %d. Should be: %d", (int) tp, (int) BSON_INT32);
        return;
      }

      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      if (strcmp(name, "value") != 0) {
        LOGE(TAG, "Invalid element name: '%s'. Should be 'value'", name);
        return;
      }

      value = bson_get_element_value_int32(it, &it);
    }

    led_set_duty(hal, value);
  }
}

static void
led_stop(void* peripheral) {
  led_t* hal = (led_t*) ((peripheral_t*) peripheral)->hal;
  led_set_duty(hal, 0);
}

static void
led_start(void* peripheral) {}

////////////
// MODULE //
////////////

static int
led_contructor(peripheral_t* peripheral, char const* it) {
  led_t* hal = (led_t*) malloc(sizeof(led_t));
  memset(hal, 0, sizeof(led_t));
  peripheral->hal = hal;

  peripheral->declaration = led_declare;
  peripheral->start       = led_start;
  peripheral->stop        = led_stop;
  peripheral->callback    = led_callback;

  char const* color = NULL;
  int32_t gpio      = -1;
  {
    bson_element_t type = bson_get_element_type(it, &it);
    while (type != BSON_END) {
      uint32_t name_size;
      char const* name = bson_get_element_name(it, &name_size, &it);

      /****/ if (strcmp(name, "color") == 0) {
        if (type != BSON_STRING) {
          LOGE(TAG, "led.color should by a BSON_STRING");
          goto fail_free_hal;
        }

        uint32_t color_len = 0;

        color = bson_get_element_value_string(it, &color_len, &it);
        LOGD(TAG, "Setting led '%s' color to '%s'", peripheral->name, color);
      } else if (strcmp(name, "gpio") == 0) {
        if (type != BSON_INT32) {
          LOGE(TAG, "led.gpio should by a BSON_INT32");
          goto fail_free_hal;
        }

        gpio = bson_get_element_value_int32(it, &it);
        LOGD(TAG, "Setting led '%s' gpio to '%d'", peripheral->name, (int) gpio);
      }

      type = bson_get_element_type(it, &it);
    }
  }

  led_init(hal, gpio, color);
  return 0;

fail_free_hal:
  led_deinit(hal);
  free(hal);

  memset(peripheral, 0, sizeof(*peripheral));

  return 1;
}

static int
led_destructor(peripheral_t* peripheral) {
  led_t* hal = (led_t*) peripheral->hal;

  if (hal->color) free((void*) hal->color);

  free(hal);
  peripheral->hal = NULL;

  return 0;
}

void
led_module_declare(void) {
  MODULE_DECLARE(led, led_contructor, led_destructor);
}