#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <bson.hpp>

static bool
isValidName(std::string const& name) {
  if (name.empty()) return false;
  if (!std::isalpha(name[0]) && name[0] != '_') return false;
  for (char c : name) {
    if (!std::isalnum(c) && c != '_') return false;
  }

  return true;
}

static std::string
parseString(std::string const& value, bool* ok) {
  if (!value.empty() && value.front() == '"' && value.back() == '"') {
    if (ok) *ok = true;
    return value.substr(1, value.size() - 2);
  }

  if (ok) *ok = false;
  return "";
}

static bool
parseBool(std::string const& value, bool* ok) {
  bool result = false;
  if (value == "true") {
    result = true;
  } else if (value == "false") {
    result = false;
  } else {
    if (ok) *ok = false;
    return result;
  }

  if (ok) *ok = true;
  return result;
}

static int32_t
parseInt32(std::string const& value, bool* ok, bool* overflow) {
  if (!std::isdigit(value[0]) && value[0] != '-') {
    if (ok) *ok = false;
    if (overflow) *overflow = false;
    return 0;
  }

  int32_t result  = atoll(value.c_str());
  char buffer[64] = {0};
  if (sizeof(int32_t) == sizeof(int)) {
    snprintf(buffer, sizeof(buffer), "%d", (int) result);
  } else if (sizeof(int32_t) == sizeof(short)) {
    snprintf(buffer, sizeof(buffer), "%hd", (short) result);
  }

  if (value != buffer) {
    if (ok) *ok = false;
    if (overflow) *overflow = true;
  } else {
    if (ok) *ok = true;
    if (overflow) *overflow = false;
  }

  return result;
}

static int64_t
parseInt64(std::string const& value, bool* ok, bool* overflow) {
  if (!std::isdigit(value[0]) && value[0] != '-') {
    if (ok) *ok = false;
    if (overflow) *overflow = false;
    return 0;
  }

  int64_t result  = atoll(value.c_str());
  char buffer[64] = {0};
  if (sizeof(int64_t) == sizeof(long)) {
    snprintf(buffer, sizeof(buffer), "%ld", (long) result);
  } else if (sizeof(int64_t) == sizeof(int)) {
    snprintf(buffer, sizeof(buffer), "%d", (int) result);
  }

  if (value != buffer) {
    if (ok) *ok = false;
    if (overflow) *overflow = true;
  } else {
    if (ok) *ok = true;
    if (overflow) *overflow = false;
  }

  return result;
}

static double
parseDouble(std::string const& value, bool* ok) {
  char* end     = NULL;
  double result = strtod(value.c_str(), &end);
  if (ok) *ok = (end == value.c_str() + value.size());

  return result;
}

static bool
parseDefinitionLine(char const* line, std::string& name, std::string& type) {
  std::vector<std::string> words;
  words.push_back("");

  bool prevIsSpace = true;

  char const* it = line;
  while (*it) {
    switch (*it) {
      case ' ':
      case ':': {
        if (!prevIsSpace) {
          prevIsSpace = true;
          words.push_back("");
        }

        break;
      }

      default: {
        words.back() += *it;
        prevIsSpace = false;
        break;
      }
    }

    ++it;
  }

  if (words.size() < 2) {
    std::cerr << "Not enough words on line" << std::endl;
    return false;
  }

  name = words[0];
  type = words[1];

  return true;
}

static bool
parseAttributeLine(char const* line, bson::Object& obj) {
  std::vector<std::string> words;
  words.push_back("");

  bool prevIsEscape = true;
  bool prevIsSpace  = true;
  bool isString     = false;
  char const* it    = line;

  while (*it) {
    if (*it == '\\' && !prevIsEscape) {
      prevIsEscape = true;
      goto next;
    }

    if (isString) {
      if (!prevIsEscape && *it == '"') isString = false;
      prevIsEscape = false;
      words.back() += *it;
    } else {
      switch (*it) {
        case '"': {
          prevIsSpace  = false;
          prevIsEscape = false;
          isString     = true;
          words.back() += *it;
          break;
        }

        case ' ': {
          if (!prevIsSpace) {
            prevIsSpace = true;
            words.push_back("");
          }

          break;
        }

        default: {
          words.back() += *it;
          prevIsSpace = false;
          break;
        }
      }
    }

  next:
    ++it;
  }

  //for (auto const& word : words) { std::cout << "word: '" << word << "'" << std::endl; }

  if (words.size() != 4 || words[2] != "=") {
    std::cerr << "Attribute line should be:" << std::endl
              << "  <type> <name> = <value>" << std::endl;
    return false;
  }

  std::string const& type  = words[0];
  std::string const& name  = words[1];
  std::string const& value = words[3];

  if (!isValidName(name)) {
    std::cerr << "Invalid attribute name: '" << name << "'" << std::endl;
    return false;
  }

  //std::cout << "Parsing value: '" << value << "'" << std::endl;
  /****/ if (type == "string") {
    bool ok         = false;
    std::string str = parseString(value, &ok);
    //std::cout << "Parsed string: '" << str << "'" << std::endl;

    if (!ok) {
      std::cerr << "Invalid attribute value (ill-formed string): '" << value << "'" << std::endl;
      return false;
    }

    obj[name] = str.c_str();
  } else if (type == "bool") {
    bool ok      = false;
    bool boolean = parseBool(value, &ok);

    if (!ok) {
      std::cerr << "Invalid attribute value (ill-formed bool): '" << value << "'" << std::endl;
      return false;
    }

    obj[name] = boolean;
  } else if (type == "int32") {
    bool ok = false, overflow = true;
    int32_t integer = parseInt32(value, &ok, &overflow);

    if (!ok) {
      if (overflow) {
        std::cerr << "Integer overflow: '" << value << "'" << std::endl;
        return false;
      }

      std::cerr << "Invalid attribute value (ill-formed int32): '" << value << "'" << std::endl;
      return false;
    }

    obj[name] = integer;
  } else if (type == "int64") {
    bool ok = false, overflow = true;
    int64_t integer = parseInt64(value, &ok, &overflow);

    if (!ok) {
      if (overflow) {
        std::cerr << "Integer overflow: '" << value << "'" << std::endl;
        return false;
      }

      std::cerr << "Invalid attribute value (ill-formed int64): '" << value << "'" << std::endl;
      return false;
    }

    obj[name] = integer;
  } else if (type == "double") {
    bool ok     = false;
    double real = parseDouble(value, &ok);

    if (!ok) {
      std::cerr << "Invalid attribute value (ill-formed double): '" << value << "'" << std::endl;
      return false;
    }

    obj[name] = real;
  } else {
    std::cerr << "Invalid type: '" << type << "'" << std::endl
              << "It should be any of: bool, int32, int64, double" << std::endl;
  }

  return true;
}

int
main(int argc, char** argv) {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <input.dt> <output.bson>" << std::endl;
    return 1;
  }

  std::string inputFilename = argv[1];
  std::ifstream input(inputFilename);
  if (!input.is_open()) {
    std::cerr << "Unable to open input file '" << inputFilename << "'" << std::endl;
    return 2;
  }

  char buffer[512] = {0};
  bson::Object device_tree;
  bson::Object* current = NULL;
  int lineIndex         = 1;

  while (input.good()) {
    input.getline(buffer, sizeof(buffer));

    switch (buffer[0]) {
      case '\n':
      case '\r':
      case 0: {
        // Empty line
        break;
      }

      case ' ': {
        if (!current) continue;
        if (!parseAttributeLine(buffer, *current)) {
          std::cerr << "Failed to parse attribute at line " << lineIndex << ": '" << buffer << "'"
                    << std::endl;
          return 4;
        }
        break;
      }

      default: {
        std::string name, type;
        bool ok = parseDefinitionLine(buffer, name, type);

        if (ok) {
          current = &(device_tree[name] = bson::Variant(BSON_OBJECT)).asObject();

          (*current)["tp"] = type;
        } else {
          std::cerr << "Failed to parse declaration at line " << lineIndex << ": '" << buffer << "'"
                    << std::endl;
          return 5;
        }

        break;
      }
    }

    ++lineIndex;
  }

  std::cout << device_tree << std::endl;

  std::string outputFilename = argv[2];
  std::ofstream output(outputFilename);
  if (!output.is_open()) {
    std::cerr << "Unable to open output file '" << outputFilename << "'" << std::endl;
    return 3;
  }

  auto result = bson::encode(device_tree);
  output.write(result.data(), result.size());

  return 0;
}